---
title: "Gympie Information and Advice"
date: 2018-03-26T21:56:24+10:00
image: Gympie Info Logo.png
draft: false
---
# How to use this Skill
To enable, [visit Amazon](https://www.amazon.com.au/Daniel-Saunders-Gympie-News-Information/dp/B079WPN4CC/)


# What can it do?
This skill hooks into resources generously provided by [Cooloola Christian Radio](http://www.cooloolachristianradio.com.au) to give information about the Gympie Region


# What can you ask it?
> Alexa, ask Gympie Info to play Radio

This will start the *live stream* of Cooloola Christian Radio.

> Alexa, ask Gympie Info to play community notices

This will play the latest *community notices* recorded by Cooloola Christian Radio.

> Alexa, open Gympie Info and play ministry spot

This will play ministry spots--short reflections from local ministers. Whilst you're listening to a podcast you can say things like *Alexa, next* or *Alexa, previous* to listen to up to the ten latest podcasts.

# Some notes about making this Skill
I came into some issues with audio streaming from non-https connections. [Cooloola Christian Radio](http://www.cooloolachristianradio.com.au) don't have an https site, but Alexa requires https for any audio streaming. A workaround was to create my own m3u files, one which points to an mp3 stream, the other points to a single mp3 file. Once these files were created, they were put on a server which can be accessed by https.

If you're interested, you can [see the code running on glitch](https://gympie-info-skill.glitch.me).

# Got suggestions?
If you've got any suggestions for improvements, new abilities, or bug reports, please [contact me]({{< relref "contact.md" >}}).
