---
title: "Hosting on Glitch"
date: 2018-03-29T11:35:54+10:00
slug: hosting-on-glitch
image: glitcheditor.png
tags: [review, my skills, coding]
draft: false
---

Alexa skills need a server to run on. Lots of skills use [Amazon's Web Services (wikipedia)](https://en.wikipedia.org/wiki/Amazon_Web_Services) as the backbone of their skills. It make sense, that way runs the whole show. You can expect they'll make things run smoothly and quickly.

However, when I started looking at tutorials that used the AWS Lamba cloud computing, I was overwhelmed and started looking for an alternative. Which is where [glitch](https://glitch.com) came along.

Glitch is a web-based development environment. It's where you can write code software which runs *in the cloud*. Currently they only support node.js, but it seems like other languages and platforms are supported.

One of the best things about Glitch is the community. They have an [active forum](https://support.glitch.com), but even better is the 'help me' function they've developed. It's simple to use - highlight a bit of code that's got you stuck and raise your hand!

![Screenshot of Help Me](/img/posts/help_me.png)

I took advantage of this early on in writing my first skill, and within a few minutes, another user was looking at my code and offering suggestions. Just what I needed.

The only downside I've come across is sometimes my skills don't respond the first time they're invoked. I don't know if this is a problem in my coding, or because of how Glitch is setup. But this is a small annoyance in the scheme of things.
