---
title: "School Holidays Removed"
description: "2021 Edition"
tags: ["news", "skills"]
date: 2021-12-15T14:21:18+10:00
---
I've removed this skill from the Alexa Skill Store as the data provided by the various education departments is in constant flux and I don't have time to maintain it.

If you're saddened by this news [please contact me]({{<ref contact.md>}}) and we might be able to work together on a fix.
