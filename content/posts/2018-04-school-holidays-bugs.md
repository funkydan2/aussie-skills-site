---
title: "School Holidays Bugs - 2018"
description: "Bugs discovered in April 2018 "
image: bugs.jpg
tags: ["news", "skills"]
date: 2018-04-17T08:46:26+10:00
draft: false
---
Today it's the start of a new school term in Queensland, so I thought I'd fire up my skill for a test. Unfortunately, I found a bug (or two). When I asked 'Alexa, ask School Holidays how long until the holidays' she answered 'two days', which clearly wasn't right.

It turned out the bug was caused by the Queensland Department of Education's calendar not being set to the right timezone. So I've written a workaround, and now the answer is much more accurate.

I've pushed the same fix to the NSW and Victorian code, and I've done a bit of testing, and things seem to be ok. But if it's not, please [contact me.]({{< ref "/contact.md" >}})
