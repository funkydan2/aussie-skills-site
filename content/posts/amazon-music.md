---
title: "Amazon Music launches in Australia"
description: ""
slug: amazon-music
image: pexels-photo-534263.jpeg
tags: [review, music]
date: 2018-03-30T14:53:56+10:00
draft: false
---
[Amazon Music Unlimited](https://www.amazon.com/Australia-New-Zealand-Amazon-Music-Unlimited/s?ie=UTF8&page=1&rh=n%3A625023011%2Cp_n_music_subscription%3A14392236011) launched this week in Australia.

Now, I'm probably not the target market for another streaming audio service. I already own gigabytes of songs as MP3s. Even my *90's music* playlist goes for more than 5 days. That's plenty of variety.

I have used the Echo to listen to streaming radio. It's a bit fiddly. Some stations work with [TuneIn](https://tunein.com/), others with [iHeart Radio](http://www.iheartradio.com.au/), and I enjoyed being able to say *'Alexa, ask TuneIn to play [Double J](http://doublej.net.au/)'* and most of the time it works.

However, with the launch of Amazon Unlimited Music in Australia this week, I thought I'd give it a go. They're offering users a 14 day trial without a credit card, or 90 days if you hand over your payment details.

For the first attempt, I asked *'Alexa, play some happy music'*, it'd been a rough start to the day in our household. Well, things didn't get better. Alexa thought we wanted to listen to the best of German polka! (And that didn't mean Weird Al Yankovich.)

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/takver/9611671969/in/photolist-fDmkde-79NGgK-9QogZv-9Qo5rK-ps23qy-3xgoX-79Sw9W-g3mCFg-8GPmLn-d7rvFL-d7rpKs-9QqQZQ-9Qr4w5-79SvQS-9Qr3Fj-9Qr6Nq-79Swpw-9Qr46o-9Qo8hn-77c1iV-9QqHR1-77c1dR-9QqErf-79SvyW-p4P3TF-9QnU6V-9Qr55L-9YmA8e-9QqYUy-9Qo73K-e5vhfH-9QodFe-79SvJh-9QnTzZ-e5vf3z-9QnV3e-9Qr1KG-79SvX9-79NG6e-48m96Q-9QqVSC-9Qo8UB-9QnZnc-9QqNg7-iRn8K-9Qoanr-e5vf68-9Qo9eT-9Qo7NM-e5AXkL" title="Accordian player at Coburg Farmers Market"><img src="https://farm4.staticflickr.com/3691/9611671969_accbb8b599_c.jpg" width="800" height="533" alt="Accordian player at Coburg Farmers Market"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

We'll see if things get better.

---

## Update
I've tried a few more things, and it seems asking for artists rather than emotions works better. Alexa did fine playing a mix songs by [Colin Buchannan](https://colinbuchanan.com.au/), *'Alexa, play songs by Colin Buchannan'*, and the [Getty's](https://www.gettymusic.com/), *'Alexa, play music by Keith Getty'*.

I reckon I'll have a go at the full trial (I just have to say *'Alexa, I want Amazon Music'*) but will most likely cancel at the end of the trial.

(Image from [Pexels](https://www.pexels.com).)
