---
title: "School Holidays Bugs Fixed"
description: "Fixed the Bugs - I think!"
slug: 2020-05-school-holidays-bugs-fixed
image: DeathtoStock_ModernWorkshop-03.jpg
tags: ["news", "skills"]
date: 2020-05-23T12:44:00+10:00
---
I've pushed a bunch of fixes to this skill. The reason for the bugs came from [some changes to some libraries](https://github.com/peterbraden/ical.js) the skill uses and [the calendars provided by the various education departments](https://www.education.vic.gov.au/about/department/Pages/datesterm.aspx?Redirect=1).

If you come across any further bugs, please [get in touch.]({{< relref "contact.md" >}})

(Photo credit: [Death To Stock](https://deathtothestockphoto.com/))
