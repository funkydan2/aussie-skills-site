---
title: "School Holidays Reinstated"
description: "2022 Edition"
tags: ["news", "skills"]
date: 2022-07-12T17:36:58+10:00
---
[At the end of 2021 I removed the School Holidays skill from Alexa.]({{< ref "2021-12-school-holidays-removed.md">}}) Recently, I've had a free couple of evenings to fix the skill, so it's [now available again from Amazon](https://www.amazon.com.au/Daniel-Saunders-Aussie-School-Holidays/dp/B07B8ZQLMK/).

[Click here for more information about the Aussie School Holidays skill.]({{< ref "aussie-holidays.md">}})
