---
title: "School Holidays Bugs"
description: "2020 Edition"
image: bugs.jpg
tags: ["news", "skills"]
date: 2020-05-02T14:42:51+10:00
---
I've discovered a couple of bugs in the [Aussie School Holidays skill]({{< relref aussie-holidays.md >}}). I've fixed things up for NSW, but still working on the QLD issues. I'll post here when they're fixed...though with most students [*learning from home* due to COVID-19](https://www.abc.net.au/news/2020-04-13/coronavirus-queensland-schools-announcement-term-two/12141380) it's probably less significant than normal.
