---
title: "Going Global"
description: "Aussie Skills Holidays now worldwide"
slug: going-global
image: DeathtoStock_NotStock7.jpg
tags: ["news", "skills"]
date: 2018-03-31T09:36:13+10:00
draft: false
---
The [Aussie School Holidays]({{< ref "/aussie-holidays.md" >}}) skill is now available worldwide.

After a request in the [Amazon Alexa Australia Users Facebook group](https://www.facebook.com/groups/1574007559364743/?ref=br_rs) I've updated this skill to be available no matter where you've told Amazon you live.

When I first released the skill, I limited its availability to Australia. I reasoned only Australians would be interested and I didn't want to clog up the skill store for other countries. However, a few people on the Facebook group noted

1. some Australians are using the American skill store because there are some skills which would be useful in Australia, but they're not enabled, and
2. some people living in other countries may want to know what the school terms are in Australia (e.g. families spread overseas).

So if you're in Argentina or America...you can now ask Alexa about Aussie School Holidays.

(Image thanks to [Death to Stock](https://deathtothestockphoto.com).)
