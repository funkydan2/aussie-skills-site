---
title: "Contact me"
date: 2018-03-27T08:07:57+10:00
slug: contact
image: contact.jpg
draft: false
---
<form name="contact" method="POST" action="/contact-thanks" netlify>
  <p>
    <label>Your Name: <input type="text" name="name"></label>
  </p>
  <p>
    <label>Your Email: <input type="email" name="email"></label>
  </p>
  <fieldset>
    <label>Reason:</label>
    <input type="radio" name="reason" id="hol" value="Holidays"><label for="hol">Aussie Holidays</label>
    <input type="radio" name="reason" id="gym" value="Gympie"><label for="gym">Gympie Info and News</label>
    <input type="radio" name="reason" id="church" value="Church"><label for="church">Gympie Presbyterian</label>
    <input type="radio" name="reason" id="other" value="Other"><label for="other">Other</label>
  </fieldset>
  <p>
    <label>Message: <textarea name="message"></textarea></label>
  </p>
  <p>
    <button type="submit">Send</button>
  </p>
</form>
