---
title: About Me
date: 2018-03-26T21:43:12.000Z
slug: about
draft: false
---

This website pulls together information about some Alexa skills I've written. It also hosts a [blog](/posts) about using Alexa in Australia.

If you've got any questions or comments please [contact me]({{< relref "contact.md" >}})

[Acknowledgements]({{< relref "acknowledge.md" >}})
