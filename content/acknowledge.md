---
title: "Credit (where it's due)"
date: 2018-03-27T07:44:07+10:00
image: creative-commons-logo.png
slug: acknowledgements
draft: false
---
Front page banner image (Echo Dot) by [Rahul Chakraborty](https://unsplash.com/photos/nGzvZe1iWOY?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [unsplash](https://unsplash.com).

Picture of school bus was a freebie from [lightstock](https://lightstock.com).

[Creative Common logo](https://www.flaticon.com/free-icon/creative-commons-logo_3501) by [Daniel Bruce](https://www.flaticon.com/authors/daniel-bruce) used under [CC By 3.0 license](http://creativecommons.org/licenses/by/3.0/)

Site is built using [hugo](https://gohugo.io) and hosted using the power of [bitbucket](https://bitbucket.org) and [netlify](https://netlify.com)

All my skills are released under the [MIT license](https://opensource.org/licenses/MIT). Words on this site are &copy; Daniel Saunders.
