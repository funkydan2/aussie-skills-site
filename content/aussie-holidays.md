---
title: Aussie School Holidays
date: 2018-03-26 21:02:24 +1000
lastmod: 2022-07-12T07:17:31+10:00
image: AUS Holidays Logo.png
---
> This Skill is no longer available. The underlying data (provided by state education departments) was continually changed, and it too labour intensive to keep the skill up to date. If you'd like to help repair this skill, please contact me.

# Introduction

I was inspired to make this skill when I was discussing possible family holiday dates. We were wondering whether school went back on a Monday or a Tuesday, and although we could find the information by looking up the education department's website, how much more fun would it be to ask Alexa!

# Using the Skill
Currently, the skill works for Queensland, New South Wales (both Western and Eastern Regions), and will soon include Victoria.

To enable this skill [visit amazon australia](https://www.amazon.com.au/Daniel-Saunders-Aussie-School-Holidays/dp/B07B8ZQLMK/)

Once enabled, get started, by saying 'Alexa, open School Holidays and set State'.

You can also ask things like:

> Is today a school day?

> Is next Monday a school day?

> How long until the holidays?

This skill needs to know which state you want to ask about. You can set or change your state by saying _'Set State'_. If you'd like your state removed from the database say, _'Delete Account'_. For New South Wales you also need to let the skill know which _region_ you're in, which you can do by saying _'Set Region'_.

If you come across any problems using this skill or want to suggest any improvements, you can either [use the contact form]({{< relref "contact.md" >}}) or log an issue at [github](https://github.com/funkydan2/aussie_school_holidays_skill/issues).

# Privacy Policy
This skill stores your Amazon Alexa user ID and the location (state of Australia) which you provide in a database.

This database is stored securely on the same server as the code is executed on, and data is only transmitted through secure channels (HTTPS) as per Amazon's standards.

# Some notes about making the skill

One of the interesting things I came across in making this skill is that each state's education department formats their electronic calendar information differently. Queensland and Victoria mark out the first and last school day and assume you know what's happening in between. NSW blocks out the whole term but has the complexity of splitting the state into _Eastern_ and _Western_ regions.

You can [see the code for this skill running on glitch](https://aussie-school-holidays.glitch.me).

# Copyright and Warranty

Uses data provided by

* Queensland Department of Education (http://education.qld.gov.au/public_media/calendar/holidays.html)
* NSW Department of Education (https://education.nsw.gov.au/public-schools/going-to-a-public-school/calendars)
* Victorian Government (https://www.vic.gov.au/calendar.html)
  All data used under a Creative Commons-BY 4.0 license.

I provide no warranty for this skill, nor guarantees you will not need a note for the teacher!

Icon based on an icon from [Smash Icons](https://www.flaticon.com/authors/smashicons) and is licensed Creative Commons-BY 3.0
