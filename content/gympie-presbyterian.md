---
title: "Gympie Presbyterian Skill"
date: 2018-03-27T07:57:36+10:00
draft: false
---
To enable this skill [visit Amazon](https://www.amazon.com.au/Daniel-Saunders-Gympie-Presbyterian-Church/dp/B079TPCDYV/)

# What can you ask it?
The most useful feature for people who aren't the minister or elder is

> Alexa, ask Gympie Presbyterian to play sermon.

which (unsurprisingly) plays the most recently uploaded sermon.

You can also query the service planning database, to find out things like sermon titles, or bible readings for a particular Sunday. So you can say things like

> What is this Sunday's sermon reading?

> What is next week's sermon title?

Honestly, this is a bit of a gimmick. It's a 50/50 chance whether it's faster to ask the Echo Dot in my office these questions, or to open up Airtable and read it for myself!

# Some notes about making this skill
*Gympie Presbyterian* was the first skill I made. Although it's available publicly in the Alexa Skill store, I don't expect anyone other than myself to use it.

This skill is able to stream the sermon podcast via a feedburner podcast feed and interacts with an [Airtable](https://airtable.com) base to get up to date information about the preaching and Bible reading program at [Gympie Presbyterian Church](https://gympiepresbyterian.org.au) (the church where I minister).

You can [see the code of this skill running on glitch](https://gp-skill.glitch.me).
